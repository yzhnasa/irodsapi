package org.bio5.irods.iplugin.api;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.apache.log4j.Logger;
import org.bio5.irods.iplugin.bean.IPlugin;
import org.bio5.irods.iplugin.bean.TapasCoreFunctions;
import org.bio5.irods.iplugin.connection.IrodsConnection;
import org.bio5.irods.iplugin.fileoperations.FileOperations;
import org.bio5.irods.iplugin.views.DirectoryContentsWindow;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.connection.IRODSSession;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.IRODSFileSystemAOImpl;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.CollectionAndDataObjectListingEntry;

public class IRodsPlugin extends JFrame{
	private String username = null;
	private String password = null;
	private int port;
	private String host = null;
	private String zone = null;
	private boolean isHomeDirectoryTheRootNode;
	private static IPlugin iplugin;
	private IRODSAccount irodsAccount = null;
	private IRODSFileFactory iRODSFileFactory = null;
	static Logger log = Logger.getLogger(IRodsPlugin.class.getName());
	public IRODSFileSystem irodsFileSystem;
	private IRODSFileSystemAOImpl iRODSFileSystemAOImpl;
	private DirectoryContentsWindow directoryContentsPane;
	private JMenuBar menuBar;
	
	public IRodsPlugin(){
		iplugin = new IPlugin();
		initIrodsFileSystem();
	}
	
	public IRodsPlugin(String username, String password, int port, String host, String zone, boolean isHomeDirectoryTheRootNode) throws ConnectException, SocketTimeoutException, MalformedURLException, JargonException{
		this.username = username;
		this.password = password;
		this.port = port;
		this.host = host;
		this.zone = zone;
		this.isHomeDirectoryTheRootNode = isHomeDirectoryTheRootNode;
		iplugin = new IPlugin();
		initIrodsFileSystem();
		if(isHomeDirectoryTheRootNode){
			this.iplugin.setHomeDirectoryTheRootNode(true);
		}
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		iplugin.setIRodsPluginMainFrame(this);
		initMainFrame();
		loginMethod();
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPort(int port){
		this.port = port;
	}
	
	public int getPort(){
		return port;
	}
	
	public void setHost(String host){
		this.host = host;
	}
	
	public String getHost(){
		return host;
	}
	
	public void setZone(String zone){
		this.zone = zone;
	}
	
	public String getZone(){
		return zone;
	}
	
	public void setHomeDirectoryTheRootNodeTrue(){
		isHomeDirectoryTheRootNode = true;
		this.iplugin.setHomeDirectoryTheRootNode(true);
	}
	
	public void setHomeDirectoryTheRootNodeFalse(){
		isHomeDirectoryTheRootNode = false;
		this.iplugin.setHomeDirectoryTheRootNode(false);
	}
	
	public boolean isHomeDirectoryTheRootNode(){
		return isHomeDirectoryTheRootNode;
	}
	
	public IPlugin getIPlugin(){
		return iplugin;
	}
	
	public void accountLogin(String username, String password, int port, String host, String zone, boolean isHomeDirectoryTheRootNode) throws ConnectException, SocketTimeoutException, MalformedURLException, JargonException{
		this.isHomeDirectoryTheRootNode = isHomeDirectoryTheRootNode;
		if(isHomeDirectoryTheRootNode){
			this.iplugin.setHomeDirectoryTheRootNode(true);
		}
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		iplugin.setIRodsPluginMainFrame(this);
		loginMethod();
	}
	
	private void irodsFileFactoryCreation() {
		try {
			this.iRODSFileFactory = TapasCoreFunctions
					.getIrodsAccountFileFactory(this.iplugin);

			this.iplugin.setiRODSFileFactory(this.iRODSFileFactory);
		} catch (JargonException jargonException) {
			log.error("Error while creating irodsFileFactory"
					+ jargonException.getMessage());
		}
	}
	
	private void initIrodsFileSystem() {
		try {
			this.irodsFileSystem = IRODSFileSystem.instance();
			this.iplugin.setIrodsFileSystem(this.irodsFileSystem);
		} catch (JargonException e) {
			log.error("Error while retrieving irodsFileSystem" + e.getMessage());
		}
	}
	
	private void setVisibilityOfForm() {
		setContentPane(this.directoryContentsPane);
		this.directoryContentsPane.setPreferredSize(getPreferredSize());
		this.directoryContentsPane.setMinimumSize(getMinimumSize());
		validate();
		repaint();
		pack();
		setVisible(true);
	}
	
	public Dimension getMinimumSize() {
		return new Dimension(200, 100);
	}

	public Dimension getPreferredSize() {
		return new Dimension(800, 600);
	}
	
	private void loginMethod() throws JargonException, ConnectException, SocketTimeoutException, MalformedURLException{
		irodsAccount = IrodsConnection.irodsConnection(
				username, password, zone, host, port);
		iplugin.setIrodsAccount(irodsAccount);
		if(iplugin.getIrodsAccount() != null){
			irodsFileFactoryCreation();
		}
		
		if (null != this.irodsFileSystem) {
			IRODSSession iRODSSession = this.irodsFileSystem
					.getIrodsSession();

			this.iplugin.setiRODSSession(iRODSSession);
		} else {
			log.error("iRODSSession is null");
		}
		
		if ((this.iplugin.getIrodsAccount() != null)
				&& (this.iplugin.getiRODSSession() != null)){
			this.iRODSFileSystemAOImpl = new IRODSFileSystemAOImpl(
					this.iplugin.getiRODSSession(),
					this.iplugin.getIrodsAccount());
			if (null != this.iRODSFileSystemAOImpl) {
				this.iplugin
						.setiRODSFileSystemAOImpl(this.iRODSFileSystemAOImpl);
			} else {
				log.error("iRODSFileSystemAOImpl is null");
			}
		}
		List<CollectionAndDataObjectListingEntry> collectionsUnderGivenAbsolutePath = FileOperations
				.setIrodsFile(null, this.iplugin,
						this.iplugin.isHomeDirectoryTheRootNode());
		iplugin.setCollectionsUnderGivenAbsolutePath(collectionsUnderGivenAbsolutePath);
		//initMainFrame();
		this.directoryContentsPane = new DirectoryContentsWindow(
				this.iplugin);
		this.iplugin.setDirectoryContentsPane(this.directoryContentsPane);
		this.directoryContentsPane.init();
		this.directoryContentsPane.implementation();
		setVisibilityOfForm();
		show();
	}
	
	private void initMainFrame(){
		setTitle("Tapas");
		setDefaultCloseOperation(2);
		setBounds(100, 100, 682, 454);

		JMenu mnNewMenu_File = new JMenu("File");
		mnNewMenu_File.setMnemonic('F');
		menuBar.add(mnNewMenu_File);

		JMenuItem mntmNewMenuItem_Open = new JMenuItem("Open");
		mntmNewMenuItem_Open.setAccelerator(KeyStroke.getKeyStroke(79, 2));

		mnNewMenu_File.add(mntmNewMenuItem_Open);

		JMenuItem mntmNewMenuItem_Exit = new JMenuItem("Exit");
		mntmNewMenuItem_Exit.setAccelerator(KeyStroke.getKeyStroke(115, 8));

		mntmNewMenuItem_Exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		mnNewMenu_File.add(mntmNewMenuItem_Exit);
		JMenu mnHelp = new JMenu("Help");
		mnHelp.setMnemonic('H');
		menuBar.add(mnHelp);
		JMenuItem mntm_About = new JMenuItem("About iPlugin");
		mntm_About.setAccelerator(KeyStroke.getKeyStroke(122, 0));
		mntm_About.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane
						.showMessageDialog(
								null,
								"Tapas V1.0 - Plugin for imagej to handle file operations with iRODS Data Servers",
								"About Tapas - ImageJ Plugin v1.0", 1);
			}
		});
		mnHelp.add(mntm_About);
	}
	
}
